$(document).ready(function(){

    const selectTabs = document.querySelector('.select-tabs');
    const choicesTabs = new Choices(selectTabs, {
        silent: true,
        searchEnabled: false,
        shouldSort: false,
    });

    const selectSort = document.querySelector('.js-choice-sort');
    const choicesSort = new Choices(selectSort, {
        silent: true,
        searchEnabled: false,
        shouldSort: false,

    });

    let tabItem = $('.tab__item'),
        sortItem = $('.sort__item'),
        subItemTim = 0;

    tabItem.on('click', function(e){
        tabItem.removeClass('tab__item-active');
        $(this).addClass('tab__item-active');
        if($(this).hasClass('tab__item-more')){
            $('.sub-tabs__items').css({'display': ''});
            $(this).children('.sub-tabs__items').css({'display': 'block'});
            if(subItemTim > 0)
                clearTimeout(subItemTim);
            subItemTim = setTimeout(function(){
                $('.sub-tabs__items').css({'display': ''});
            }, 2000)
        }
    });


    sortItem.on('click', function(e){
        sortItem.removeClass('sort__item-active');
        $(this).addClass('sort__item-active');
    });

    let tabsWidth = 0;
    for(let i = 0; i < tabItem.length; i++){
        tabsWidth += $(tabItem[i]).width() + parseInt($(tabItem[i]).css('marginRight'), 10);
        if(tabsWidth > $('.tabs__items').width()){
            let subTubItem = document.createElement('a');
            subTubItem.setAttribute('href', tabItem[i-1].getAttribute("href"));
            subTubItem.classList.add('sub-tab__item');
            subTubItem.innerText = tabItem[i-1].innerText;
            $(".tab__item-more .sub-tabs__items").append(subTubItem);
            $(tabItem[i-1]).css({"display":"none"})
            $(".tab__item-more").css({"display":"block"});
        }
    }

});