$('.main-slider').slick({
    dots: true,
    infinite: true,
    speed: 500,
    autoplay: true,
    lazyLoad: 'ondemand'
})


$('.slick-dots li button').on('click', function(e){
    e.stopPropagation();
});